function New-CustomChannel{
    [CmdletBinding()]
    param(
    [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
    [string] $GroupID,
    [Parameter(Mandatory,ValueFromPipelineByPropertyName)]
    [string] $ChannelName
    )    
    Process{
    New-TeamChannel -GroupId $GroupID -DisplayName $ChannelName     
    }    
}
